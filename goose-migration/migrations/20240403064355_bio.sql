-- +goose Up
-- +goose StatementBegin

CREATE TABLE IF NOT EXISTS bio (
                              id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                              name varchar(100) NOT NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS bio
-- +goose StatementEnd
