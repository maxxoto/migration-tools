package migrations

import (
	"context"
	"database/sql"

	"github.com/pressly/goose/v3"
)

func init() {
	goose.AddMigrationContext(upTest, downTest)
}

func upTest(ctx context.Context, tx *sql.Tx) error {
	sql := `CREATE TABLE IF NOT EXISTS profilesOld (
		id INT NOT NULL AUTO_INCREMENT,
		name VARCHAR(100) NOT NULL,
		created_at timestamp NOT NULL DEFAULT now(),
		updated_at timestamp NOT NULL DEFAULT now(),
		CONSTRAINT profile_pk PRIMARY KEY (id)
		)`
	_, err := tx.ExecContext(ctx, sql)
	return err
}

func downTest(ctx context.Context, tx *sql.Tx) error {
	// This code is executed when the migration is rolled back.
	return nil
}
