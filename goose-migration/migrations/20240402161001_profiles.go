package migrations

import (
	"context"
	"database/sql"

	"github.com/pressly/goose/v3"
)

func init() {
	goose.AddMigrationContext(upProfiles, downProfiles)
}

func upProfiles(ctx context.Context, tx *sql.Tx) error {
	sql := `CREATE TABLE IF NOT EXISTS profiles (
		id INT NOT NULL AUTO_INCREMENT,
		name VARCHAR(100) NOT NULL,
		created_at timestamp NOT NULL DEFAULT now(),
		updated_at timestamp NOT NULL DEFAULT now(),
		CONSTRAINT profile_pk PRIMARY KEY (id)
		)`
	_, err := tx.ExecContext(ctx, sql)
	return err
}

func downProfiles(ctx context.Context, tx *sql.Tx) error {
	sql := `DROP TABLE IF EXISTS profiles`
	_, err := tx.ExecContext(ctx, sql)
	return err
}
