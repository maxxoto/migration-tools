package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"log"
	"os"

	_ "goose-migration/migrations"

	_ "github.com/go-sql-driver/mysql"
	"github.com/pressly/goose/v3"
)

type DBString struct {
	Name     string
	DbString string
}

var (
	flags = flag.NewFlagSet("goose", flag.ExitOnError)
	dir   = flags.String("dir", "./migrations", "directory with migration files")
	// DB Selection
	predefinedDB = []DBString{
		{
			Name:     "main",
			DbString: "dev-internet:toor@tcp(10.24.0.1:3306)/goose_main",
		},
		{
			Name:     "second",
			DbString: "dev-internet:toor@tcp(10.24.0.1:3306)/goose_second",
		},
	}
)

func main() {
	ctx := context.Background()
	flags.Usage = usage
	flags.Parse(os.Args[1:])

	args := flags.Args()

	if len(args) > 1 && args[0] == "run" {
		log.Printf("PROGRAM RUN\n") //
		os.Exit(0)
	}

	if len(args) > 1 && args[0] == "create" {
		if err := goose.RunContext(ctx, "create", nil, *dir, args[1:]...); err != nil {
			log.Fatalf("goose run: %v", err)
		}
		return
	}

	if len(args) < 3 {
		flags.Usage()
		return
	}

	if args[0] == "-h" || args[0] == "--help" {
		flags.Usage()
		return
	}

	driver, dbstring, command := args[0], args[1], args[2]

	switch driver {
	case "postgres", "mysql", "sqlite3", "redshift":
		if err := goose.SetDialect(driver); err != nil {
			log.Fatal(err)
		}
	default:
		log.Fatalf("%q driver not supported\n", driver)
	}

	switch dbstring {
	case "":
		log.Fatalf("-dbstring=%q not supported\n", dbstring)
	default:
		// To check if inputted dbString is predefinedDB
		for i := 0; i < len(predefinedDB); i++ {
			db := predefinedDB[i]
			if dbstring == db.Name {
				dbstring = db.DbString
				break
			}
		}
	}

	if driver == "redshift" {
		driver = "postgres"
	}

	db, err := sql.Open(driver, dbstring)
	if err != nil {
		log.Fatalf("-dbstring=%q: %v\n", dbstring, err)
	}

	arguments := []string{}
	if len(args) > 3 {
		arguments = append(arguments, args[3:]...)
	}

	if err := goose.RunContext(ctx, command, db, *dir, arguments...); err != nil {
		log.Fatalf("goose run: %v", err)
	}
}

func usage() {
	var fullCommand string
	var predefinedUsage = "\n\n\tPredefined DB: \n"

	fullCommand += usagePrefix

	for i := 0; i < len(predefinedDB); i++ {
		db := predefinedDB[i]

		predefinedUsage += fmt.Sprintf("\t - %s : %s", db.Name, db.DbString+"\n")
	}

	fullCommand += predefinedUsage + exampleUsage + usageCommands

	log.Print(fullCommand)
	flags.PrintDefaults()
}

var (
	usagePrefix = `
	Usage: goose [OPTIONS] DRIVER DBSTRING/PREDEFINED_DB COMMAND
	Drivers:
		postgres
		mysql
		sqlite3
		redshift`

	exampleUsage = `
	Examples:
		goose mysql "user:password@/dbname?parseTime=true" status
		goose mysql my_predefined_db status
		goose sqlite3 ./foo.db status
		goose postgres "user=postgres dbname=postgres sslmode=disable" status
		goose redshift "postgres://user:password@qwerty.us-east-1.redshift.amazonaws.com:5439/db" status`

	usageCommands = `
	Commands:
		up                   Migrate the DB to the most recent version available
		up-to VERSION        Migrate the DB to a specific VERSION
		down                 Roll back the version by 1
		down-to VERSION      Roll back to a specific VERSION
		redo                 Re-run the latest migration
		status               Dump the migration status for the current DB
		version              Print the current version of the database
		create NAME [sql|go] Creates new migration file with next version

	Options:
`
)
