-- Revert sqitcher-migration:profile from mysql

BEGIN;

DROP TABLE IF EXISTS profiles;

COMMIT;
