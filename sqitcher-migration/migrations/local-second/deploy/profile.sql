-- Deploy sqitcher-migration:profile to mysql

BEGIN;

CREATE TABLE IF NOT EXISTS profiles (
                              id INT NOT NULL AUTO_INCREMENT,
                              name varchar(100) NOT NULL,
                              created_at timestamp NOT NULL DEFAULT now(),
                              updated_at timestamp NOT NULL DEFAULT now(),
                              CONSTRAINT profile_pk PRIMARY KEY (id)
);

COMMIT;
