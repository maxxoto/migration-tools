import { Knex } from 'knex';
import { faker } from '@faker-js/faker';

const tableName = 'profiles';
const maxRecords = 50;

export async function seed(knex: Knex): Promise<void> {
  // Inserts seed entries
  for (let i = 0; i < maxRecords; i++) {
    await knex(tableName).insert([
      {
        name: faker.person.fullName(),
        age: Math.floor(Math.random() * (40 - 17 + 1)) + 17,
      },
    ]);
  }
}
