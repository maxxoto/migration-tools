import { Knex } from 'knex';

const tableName = 'profiles';
export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex(tableName).del();

  // Inserts seed entries
  await knex(tableName).insert([
    {
      name: 'test',
      age: 10,
    },
  ]);
}
