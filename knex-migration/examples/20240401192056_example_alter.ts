import type { Knex } from 'knex';

const tableName = 'profiles';
export async function up(knex: Knex): Promise<void> {
  return knex.schema.table(tableName, (table) => {
    table.integer('age').notNullable();
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.table(tableName, function (table) {
    table.dropColumn('age');
  });
}
