import type { Knex } from 'knex';

//Change this
const tableName = 'yourTable';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable(tableName, (table) => {

  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable(tableName);
}
