import type { Knex } from 'knex';

const tableName = 'profiles';
export async function up(knex: Knex): Promise<void> {
  // return knex.schema.table(tableName, (table) => {
  //   table.integer('age').notNullable();
  // });

  return knex.schema
    .raw('ALTER TABLE profiles ADD pic VARCHAR(50)')
    .raw('ALTER TABLE profiles ADD username VARCHAR(50)');
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.table(tableName, function (table) {
    table.dropColumn('age');
  });
}
