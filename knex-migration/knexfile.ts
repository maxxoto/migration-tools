import type { Knex } from 'knex';
import * as dotenv from 'dotenv';

dotenv.config();

const config: { [key: string]: Knex.Config } = {
  // Default
  main: {
    client: 'mysql2',
    connection: {
      database: process.env.DB_MAIN_NAME,
      host: process.env.DB_MAIN_HOST,
      port: parseInt(process.env.DB_MAIN_PORT),
      user: process.env.DB_MAIN_USER,
      password: process.env.DB_MAIN_PASSWORD,
    },
    pool: { min: 0, max: 10 },
    migrations: {
      directory: 'migrations/main',
      stub: 'config/migrations.stub',
    },
    seeds: {
      directory: 'seeds/main',
      stub: 'config/seeders.stub',
    },
  },

  // NOTE: CHANGE THIS
  auth: {
    client: 'mysql2',
    connection: {
      database: process.env.DB_SECOND_NAME,
      host: process.env.DB_SECOND_HOST,
      port: parseInt(process.env.DB_SECOND_PORT),
      user: process.env.DB_SECOND_USER,
      password: process.env.DB_SECOND_PASSWORD,
    },
    migrations: {
      directory: 'migrations/auth',
      stub: 'config/migrations.stub',
    },
    seeds: {
      directory: 'seeds/auth',
      stub: 'config/seeders.stub',
    },
  },
};

module.exports = config;
