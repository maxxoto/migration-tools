#!/usr/bin/env node

import * as fs from 'fs';

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

if (process.argv.length < 3) {
  console.error('Usage: generate:factory <file_name>');
  process.exit(1);
}

const path = 'src/database/factories/';
const fileName: string = capitalizeFirstLetter(process.argv[2]);
const factoryFilename = `${fileName}Factory.ts`;

const seederTemplate = `
import { setSeederFactory } from 'typeorm-extension';

export default setSeederFactory(MyModel, async (faker) => {});


 `;

fs.writeFileSync(path + factoryFilename, seederTemplate);

console.log(`Factory file "${factoryFilename}" created successfully!`);
