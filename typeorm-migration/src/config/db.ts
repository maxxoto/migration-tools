import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';

const defaultOptions: TypeOrmModuleOptions = {
  type: 'mysql',
  logging: [
    'log',
    'info',
    'warn',
    'error',
    // 'query'
  ],
  autoLoadEntities: false,
  synchronize: false,
  maxQueryExecutionTime: 3000,
  timezone: 'Z',
};

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule.forRoot()],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        ...defaultOptions,
        type: 'mysql',
        host: configService.get('DB_MAIN_HOST'),
        port: configService.get('DB_MAIN_PORT'),
        username: configService.get('DB_MAIN_USER'),
        password: configService.get('DB_MAIN_PASSWORD'),
        database: configService.get('DB_MAIN_NAME'),
        entities: [__dirname + '/modules/**/entities/*.entity{.ts,.js}'],
      }),
    }),

    TypeOrmModule.forRootAsync({
      name: 'secondDB',
      imports: [ConfigModule.forRoot()],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        ...defaultOptions,
        type: 'mysql',
        host: configService.get('DB_SECOND_HOST'),
        port: configService.get('DB_SECOND_PORT'),
        username: configService.get('DB_SECOND_USER'),
        password: configService.get('DB_SECOND_PASSWORD'),
        database: configService.get('DB_SECOND_NAME'),
        entities: [__dirname + '/modules/**/entities/*.entity{.ts,.js}'],
      }),
    }),
  ],
})
export class DatabaseModule {}
