#!/usr/bin/env node

import * as fs from 'fs';

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

if (process.argv.length < 3) {
  console.error('Usage: generate:seeder <file_name>');
  process.exit(1);
}

const path = 'src/database/seeders/';
const fileName: string = capitalizeFirstLetter(process.argv[2]);
const timestamp: string = Math.floor(new Date().getTime() / 1000).toString();
const seederFilename = `${timestamp}-${fileName}Seeder.ts`;

const seederTemplate = `
import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';

export default class ${fileName}Seeder implements Seeder {
    public async run(
      dataSource: DataSource,
      factory: SeederFactoryManager,
    ): Promise<void> {
    }
  }

 `;

fs.writeFileSync(path + seederFilename, seederTemplate);

console.log(`Seeder file "${seederFilename}" created successfully!`);
