import { faker } from '@faker-js/faker';
import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';
import { ProfilesSecond } from '../../../modules/profiles/entities/profileSecond.entity';

export default class ProfileSecondSeeder implements Seeder {
  public async run(): Promise<void> {
    const profile = new ProfilesSecond();
    profile.name = faker.person.fullName();
    await profile.save();
  }
}
