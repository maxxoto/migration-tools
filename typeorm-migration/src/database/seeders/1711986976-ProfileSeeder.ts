import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';
import { Profiles } from '../../modules/profiles/entities/profile.entity';
import { faker } from '@faker-js/faker';

export default class ProfileSeeder implements Seeder {
  public async run(
    _: DataSource,
    factory: SeederFactoryManager,
  ): Promise<void> {
    const profile = new Profiles();
    profile.name = faker.person.fullName();
    await profile.save();

    // Using factory
    const profileFactory = await factory.get(Profiles);
    await profileFactory.saveMany(10);
  }
}
