import { DataSource, DataSourceOptions } from 'typeorm';
import { SeederOptions } from 'typeorm-extension';
import * as dotenv from 'dotenv';

// Load env file
dotenv.config();

const db1Config: DataSourceOptions & SeederOptions = {
  migrationsTableName: 'migration_metadata',
  type: 'mysql',
  host: process.env.DB_MAIN_HOST,
  port: parseInt(process.env.DB_MAIN_PORT),
  username: process.env.DB_MAIN_USER,
  password: process.env.DB_MAIN_PASSWORD,
  database: process.env.DB_MAIN_NAME,
  entities: ['src/modules/**/entities/**.entity{.ts,.js}'],
  migrations: ['src/database/migrations/*{.ts,.js}'],
  seeds: ['src/database/seeders/*{.ts,.js}'],
  factories: ['src/database/factories/*{.ts,.js}'],
};

export const db1 = new DataSource(db1Config);
