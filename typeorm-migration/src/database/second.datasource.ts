import { DataSource, DataSourceOptions } from 'typeorm';
import { SeederOptions } from 'typeorm-extension';
import * as dotenv from 'dotenv';

// Load env file
dotenv.config();

const db2Config: DataSourceOptions & SeederOptions = {
  migrationsTableName: 'migration_metadata',
  type: 'mysql',
  host: process.env.DB_SECOND_HOST,
  port: parseInt(process.env.DB_SECOND_PORT),
  username: process.env.DB_SECOND_USER,
  password: process.env.DB_SECOND_PASSWORD,
  database: process.env.DB_SECOND_NAME,
  entities: ['src/modules/**/entities/**.entity{.ts,.js}'],
  migrations: ['src/database/migrations/secondDB/*{.ts,.js}'],
  seeds: ['src/database/seeders/secondDB/*{.ts,.js}'],
  factories: ['src/database/factories/secondDB/*{.ts,.js}'],
};
export const db2 = new DataSource(db2Config);
