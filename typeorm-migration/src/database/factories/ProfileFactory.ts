import { setSeederFactory } from 'typeorm-extension';
import { Profiles } from '../../modules/profiles/entities/profile.entity';

export default setSeederFactory(Profiles, async (faker) => {
  const profile = new Profiles();
  profile.name = faker.person.fullName();
  return profile;
});
