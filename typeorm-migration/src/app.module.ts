import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProfilesModule } from './modules/profiles/profiles.module';
import { BaseService } from './modules/base/base.service';
import { DatabaseModule } from './config/db';

@Module({
  imports: [DatabaseModule, ProfilesModule],
  controllers: [AppController],
  providers: [AppService, BaseService],
})
export class AppModule {}
